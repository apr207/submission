#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Oct  2 14:31:00 2019

@author: andrew
"""

import numpy as np
from pyDOE import lhs as LHS
import subprocess
import os
import time

#import sys
from os.path import expanduser

# home directory
home = expanduser("~")

# Imports for Optimiser
import IscaOpt
from hydro_plane_correct import Ellipse

from multiprocessing import Process, Queue, current_process, freeze_support
    
current = os.getcwd()   

import shutil


# APR added
try:
    from interfaces import EllipseInterface
except:
    from .interfaces import EllipseInterface
try:
    from base_class import Problem
except:
    from .base_class import Problem
try:
    from data import support
except:
    from .data import support

from datetime import date

from pathlib import Path

import fasteners
from scipy.stats import dirichlet as D
#from PyFoam.Execution.UtilityRunner import UtilityRunner
#
#from os.path import basename
#import glob
#import math as mt
#from PyFoam.RunDictionary.ParsedParameterFile import ParsedParameterFile
#import re

#############
# Environment
############# 
# On ISCA, there can be batches of up to the maximum number of samples

#environment = 'isambard'
#environment = 'isambard_test'
environment = 'isca'
#environment = 'isca_test'

no_of_nodes = 10

# Locally, there can only be three "nodes" as each one has 2 CPUS, and I only have 8 CPUs
#environment = 'local'
#no_of_nodes = 3

##########
# Sampling
##########
#sampling = 'manual'
sampling = 'latin'


##########################
# Selection of bash script
##########################

# this bash script is for the OpenFOAM run
if environment == 'isca':    
    bash = 'isca_parallel.sh'
elif environment == 'isca_test':
    bash = 'isca_parallel_test.sh'     
elif environment == 'isambard':
    bash = 'isambard_parallel.sh'
elif environment == 'isambard_test':
    bash = 'isambard_parallel_test.sh'    
else:
    bash = 'local_parallel.sh'    # this doesn't exist!!
    
############################################################
# HeadCell class used by BOTH INITIAL_SAMPLING AND OPTIMISER
############################################################

class HeadCell(Problem, EllipseInterface):

    def __init__(self, settings):
        self.source_case = settings.get('source_case', './data/HeadCell/source/')            
        self.case_path = settings.get('case_path', './data/HeadCell/case_local/')
        self.mesh_path = settings.get('mesh_path', './data/HeadCell/meshes/')
        self.mesh_failures_path = settings.get('mesh_failures_path', './data/HeadCell/mesh_failures/')
        self.setup()
        

    def setup(self, verbose=False):
        """
        Just sets values
        """
        self.L = 4.0
        self.A = 45.0
        self.R = 1.0
        self.xlb, self.xub = -self.L, self.L
        self.zlb, self.zub = -self.L, self.L
        self.anglelb, self.angleub = 0, self.A
        self.majorlb, self.majorub = 4.0*self.R, 8.0*self.R 
        self.minorlb, self.minorub = 4.0*self.R, 8.0*self.R
        self.alpha1lb, self.alpha1ub = 0, 5
        self.alpha2lb, self.alpha2ub = 0, 5
        self.alpha3lb, self.alpha3ub = 0, 5
        self.beta1lb, self.beta1ub = 0, 5
        self.beta2lb, self.beta2ub = 0, 5
        self.beta3lb, self.beta3ub = 0, 5        
        self.omega1lb, self.omega1ub = 0, 1
        self.omega2lb, self.omega2ub = 0, 1
        self.omega3lb, self.omega3ub = 0, 1  

        
        EllipseInterface.__init__(self, self.xlb, self.xub, self.zlb, self.zub, \
                                 self.anglelb, self.angleub, self.majorlb, self.majorub, \
                                 self.minorlb, self.minorub, \
                                 self.alpha1lb, self.alpha1ub, \
                                 self.alpha2lb, self.alpha2ub, \
                                 self.alpha3lb, self.alpha3ub, \
                                 self.beta1lb, self.beta1ub, \
                                 self.beta2lb, self.beta2ub, \
                                 self.beta3lb, self.beta3ub, \
                                 self.omega1lb, self.omega1ub, \
                                 self.omega2lb, self.omega2ub, \
                                 self.omega3lb, self.omega3lb)

    def info(self):
        raise NotImplementedError

    def get_configurable_settings(self):
        raise NotImplementedError

    def run(self, shape, verbose=False):
        xp, yp, rp = shape
        support.circle_to_stl(rp, xp, yp, \
            file_directory=self.case_path+self.stl_dir, file_name=self.stl_file_name, draw=False)
        t, p = self.problem.cost_function(sense="multi", verbose=verbose)
        return t, p

    def evaluate(self, decision_vector, verbose=False):
        if not self.constraint(decision_vector):
            raise ValueError('Constraint violated. Please supply a feasible decision vector.')
        shape = self.convert_decision_to_shape(decision_vector)
        try:
            return self.run(shape, verbose)
        except Exception as e:
            print('Solution evaluation failed.')
            print(e)

######################################################################
# follow(thefile) function used by BOTH INITIAL_SAMPLING AND OPTIMISER
######################################################################
            
# follow a log file:
def follow(thefile):
    thefile.seek(0,2)
    while True:
        line = thefile.readline()
        if not line:
            time.sleep(0.1)
            continue       
        yield line

###################################################################
# Average(lst) function used by BOTH INITIAL_SAMPLING AND OPTIMISER
###################################################################

# compute the average of a list:        
def Average(lst): 
    return sum(lst) / len(lst)


#############################################################
# lhs_initial_samples() FUNCTION USED BY INITAL_SAMPLING ONLY
#############################################################

def lhs_initial_samples(n_dim, ub, lb, n_samples=4, cfunc=None, cargs=(), ckwargs={}):
    """
    Generate Latin hypercube samples from the decision space using pyDOE.

    Parameters.
    -----------
    n_samples (int): the number of samples to take. 
    cfunc (method): a cheap constraint function.
    cargs (tuple): arguments for cheap constraint function.
    ckawargs (dictionary): keyword arguments for cheap constraint function. 

    Returns a set of decision vectors.         
    """
    seed = 1234
    np.random.seed(seed)
    samples = LHS(n_dim, samples=n_samples)
    
    scaled_samples = ((ub - lb) * samples) + lb            
    
    for a in range(n_samples):
        alphas = np.random.random_sample(3)*2.0
        for b in range(n_dim-9, n_dim-6):
            scaled_samples[a][b]=alphas[b-(n_dim-9)]                    
    
    for a in range(n_samples):
        betas = np.random.random_sample(3)*2.0
        for b in range(n_dim-6, n_dim-3):
            scaled_samples[a][b]=betas[b-(n_dim-6)]

    for a in range(n_samples):
        omegas = D.rvs([1]*3)[0]
        for b in range(n_dim-3, n_dim):
            scaled_samples[a][b]=omegas[b-(n_dim-3)]

    
    if cfunc is not None: # check for constraints
        print('Checking for constraints.')
        scaled_samples = np.array([i for i in scaled_samples if cfunc(i, *cargs, **ckwargs)])

    return scaled_samples



#####################################################################################
# gap_and_checkMesh_constraint() function used by BOTH INITIAL_SAMPLING AND OPTIMISER
#####################################################################################

# constaints:
def gap_and_checkMesh_constraint(x, layout):
    """
    Create meshes and check the constraints using the decision vector and layout.

    Parameters.
    -----------
    x (numpy array): the decision vector. 
    layout (Ellipse object): the object for the generation of the pointwise files. 

    Returns whether the constraint was successfully passed.         
    """
    # get the current directory
    current = os.getcwd()
    
    # defaults for success:
    gap_success = True
    mesh_success = False
    
    
    
#    #######
#    
#    mesh_success = True
#    
#    
#    
#    
#    #######
#    
    
    
    
    
    
    
    
    
    
    
    
    
    
    #convergence_success = True # this is not tested here
    success = False
    
    # checkMesh:
    utility3 = "checkMesh"
    
    # platform specific changes (not used by Isambard):
    if environment == 'blades':
        Pointwise_path='/usr/local/Pointwise/PointwiseV18.0R2/pointwise'
    elif environment == 'local':
        Pointwise_path='/home/andrew/Pointwise/PointwiseV18.0R2/pointwise'
    elif ((environment == 'isca') or (environment == 'isca_test')):    
        Pointwise_path = '/gpfs/ts0/home/apr207/Pointwise/PointwiseV18.0R2/pointwise'
    else:
        #local is default
        Pointwise_path='/home/andrew/Pointwise/PointwiseV18.0R2/pointwise'
    
    # paths:
    mesh_path = "/data/HeadCell/meshes/"
    source_path = "/data/HeadCell/source/"
    mesh_failures_path = "/data/HeadCell/mesh_failures/"
    
    # the neccessary name for the child directory:
    dir_name = "_"
    
    for j in range(len(x)):
        dir_name += "{0:.4f}_".format(x[j])
        dir_name = dir_name.replace("[","")
        dir_name = dir_name.replace("]","")

    # make the child directory:
    subprocess.call(['mkdir', '-p', current + mesh_path + dir_name + '/system' ])
    subprocess.call(['mkdir', '-p', current + mesh_path + dir_name + '/constant/polyMesh' ])


    # copy controlDict and fvSchemes for checkMesh 
    shutil.copyfile(current + source_path + 'system/controlDict', current + mesh_path + dir_name + '/system/controlDict')
    shutil.copyfile(current + source_path + 'system/fvSchemes', current + mesh_path + dir_name + '/system/fvSchemes')
    shutil.copyfile(current + source_path + 'system/fvSolution', current + mesh_path + dir_name + '/system/fvSolution')
    
    # copy all mesh files to mesh path
    for filename in os.listdir(current + source_path):
        full_file_name = os.path.join(current + source_path, filename)
        if os.path.isfile(full_file_name):
            shutil.copy(full_file_name, current + mesh_path + dir_name)

    # location of centre of ellipse:
    bottom_x_centre = x[0]
    bottom_z_centre = x[1]
    
    # rotation angle:
    rot_angle = x[2]*(np.pi/180.0)
    
    # ellipse sizes:
    a = x[3]
    b = x[4]
    
    #########################
    # Create the STL surfaces
    #########################
    
    lip="lip_correct"
    bottom="bottom_correct"
    lowercentre="lowercentre_correct"
    uppercentre="uppercentre_correct"
    top="top_correct"
    a_top = 21.25
    b_top = 21.25
    x_top = 0.0
    z_top = 0.0
    x_bottom = x[0]
    z_bottom = x[1]
    angle_bottom = x[2]
    a_bottom = x[3]
    b_bottom = x[4]

    ################################################################################
    ## TRAY 0
    ################################################################################
    tray = 0
    y = np.array([-19.1875, -15.125, -10.25, -6.59375])
    p = np.array([0, -2.9375, 0])               
    names = [bottom + "_" + str(tray), lowercentre + "_" + str(tray), uppercentre + "_" + str(tray), top + "_" + str(tray)]
    layout.correct_tray(x, y, p, names)
    file_name_tray = "tray_" + str(tray) + ".stl"
    layout.stl_tray(p[1], y[0], x_top, x_bottom, z_top, z_bottom, angle_bottom, a_bottom, b_bottom, a_top, b_top, file_name_tray, x)
    
    ################################################################################
    ## TRAY 1
    ################################################################################
    tray += 1
    y += 4.25
    p += np.array([0, 4.25, 0])               
    names = [bottom + "_" + str(tray), lowercentre + "_" + str(tray), uppercentre + "_" + str(tray), top + "_" + str(tray)]
    layout.correct_tray(x, y, p, names)
    file_name_tray = "tray_" + str(tray) + ".stl"
    layout.stl_tray(p[1], y[0], x_top, x_bottom, z_top, z_bottom, angle_bottom, a_bottom, b_bottom, a_top, b_top, file_name_tray, x)    

    ################################################################################
    ## LIP 1
    ################################################################################    
    y_lip = np.array([0.0, 6.0625])  # y_bottom, y_top
    names = lip + "_" + str(tray)
    layout.correct_lip(x, y_lip[0], names)    
    file_name_lip = "lip_" + str(tray) + ".stl"
    layout.stl_lip(p[1], y[0], x_top, x_bottom, z_top, z_bottom, angle_bottom, a_bottom, b_bottom, a_top, b_top,  \
                     p[1]-y_lip[1], y_lip[1], y_lip[0], file_name_lip, x)
    
    ################################################################################
    ## TRAY 2
    ################################################################################
    tray += 1
    y += 9.0
    p += np.array([0, 9.0, 0])               
    names = [bottom + "_" + str(tray), lowercentre + "_" + str(tray), uppercentre + "_" + str(tray), top + "_" + str(tray)]
    layout.correct_tray(x, y, p, names)
    file_name_tray = "tray_" + str(tray) + ".stl"
    layout.stl_tray(p[1], y[0], x_top, x_bottom, z_top, z_bottom, angle_bottom, a_bottom, b_bottom, a_top, b_top, file_name_tray, x)      

    ################################################################################
    ## LIP 2
    ################################################################################    
    y_lip += 9.0  # y_bottom, y_top
    names = lip + "_" + str(tray)
    layout.correct_lip(x, y_lip[0], names)    
    file_name_lip = "lip_" + str(tray) + ".stl"
    layout.stl_lip(p[1], y[0], x_top, x_bottom, z_top, z_bottom, angle_bottom, a_bottom, b_bottom, a_top, b_top,  \
                     p[1]-y_lip[1]+9.0, y_lip[1], y_lip[0], file_name_lip, x)

    ################################################################################
    ## TRAY 3
    ################################################################################
    tray += 1
    y += 9.0
    p += np.array([0, 9.0, 0])               
    names = [bottom + "_" + str(tray), lowercentre + "_" + str(tray), uppercentre + "_" + str(tray), top + "_" + str(tray)]
    layout.correct_tray(x, y, p, names)
    file_name_tray = "tray_" + str(tray) + ".stl"
    layout.stl_tray(p[1], y[0], x_top, x_bottom, z_top, z_bottom, angle_bottom, a_bottom, b_bottom, a_top, b_top, file_name_tray, x) 

    ################################################################################
    ## LIP 3
    ################################################################################    
    y_lip += 9.0  # y_bottom, y_top   
    names = lip + "_" + str(tray)
    layout.correct_lip(x, y_lip[0], names)
    file_name_lip = "lip_" + str(tray) + ".stl"
    layout.stl_lip(p[1], y[0], x_top, x_bottom, z_top, z_bottom, angle_bottom, a_bottom, b_bottom, a_top, b_top,  \
                     p[1]-y_lip[1]+18.0, y_lip[1], y_lip[0], file_name_lip, x)

    ################################################################################
    ## TRAY 4
    ################################################################################
    tray += 1
    y += 9.0
    p += np.array([0, 9.0, 0])               
    names = [bottom + "_" + str(tray), lowercentre + "_" + str(tray), uppercentre + "_" + str(tray), top + "_" + str(tray)]
    layout.correct_tray(x, y, p, names)
    file_name_tray = "tray_" + str(tray) + ".stl"
    layout.stl_tray(p[1], y[0], x_top, x_bottom, z_top, z_bottom, angle_bottom, a_bottom, b_bottom, a_top, b_top, file_name_tray, x) 
    

        
    ################################
    # Now create the Pointwise files
    ################################
    
    # create space for corners and gaps:
    corners = np.zeros((4,2))
    gaps = np.zeros(4)
    
    # the bottom of the tray:
    y_bottom = -14.9375    
    
    # very top of tray: 
    p1 = np.array([0, 6.0625, 0])
    
    # very bottom of tray:
    p2 = np.array([bottom_x_centre, y_bottom, bottom_z_centre])
    
    # the radii at the first tray layer:
    a_top = layout.get_ellipse_radii(21.25, a, -2.34375)
    b_top = layout.get_ellipse_radii(21.25, b, -2.34375)
    
    # the points at the first tray layer:
    c_top = layout.get_ellipse_points(p1, p2, -2.34375)
    
    # the angle at the first tray layer:
    angle_top = layout.get_ellipse_angles(rot_angle, -2.34375)
    
    # obtain the four corners:
    for i in range(1,5):
        angles = np.linspace(start=0.5*i*np.pi, stop=0.5*(i-1)*np.pi, num=1, endpoint=True)
        corners[i-1,:] = layout.generate_ellipse(c_top, a_top, b_top, angles, theta=angle_top+np.pi, n=0)

    # compute horizontal gaps:
    for j in range(0,4):
        gaps[j] = 21.25 - np.sqrt((corners[j,0])**2 + (corners[j,1])**2)
    
    # minimim horizontal gap:
    min_gap=min(gaps)

    # get the index of the minimum horizontal gap and the maximum x-extent:
    index = np.where(gaps == min_gap)
    x_extent_vector = abs(corners[index])
    x_extent= np.amax(x_extent_vector)
    
    # Definition of the x-y plane locations
    x1 = 21.25
    x2 = x_extent
    x3 = x_extent
    x4 = 21.25
    
    y1 = -2.9375
    y2 = -2.34375
    y3 = -6.59375
    y4 = 1.3125
    
    # Hero's formula for the normal gap
    A = y2 - y3
    B = np.sqrt((y4 - y2)**2 + (x4 - x2)**2)
    P = np.sqrt((x1 - x2)**2 + (y1 - y2)**2)
    S = (A+B+P)/2
    Area = np.sqrt(S*(S-A)*(S-B)*(S-P))
    H = 2*Area/B
    
    # constraints:
    if (H < 2.0):
        # the tray gap is less than 2 inches:
        gap_success = False
    
    # to ensure minimum_gap_success and checkMesh_success are independent, it will always try to create a mesh and check it:
    
    # update Pointwise files using decision vector (needs checking)
    layout.update(x)
    
    # decide where we are to run Pointwise:
    if ((environment == 'isambard') or (environment == 'isambard_test')):
            
        
        # change bash_pointwise in order to run from current directory
        with open(current + '/pointwise_directory.sh', 'r') as myfile:
            data_myfile = myfile.readlines()

        # read in each line and change the directory location                
        for line in range(len(data_myfile)):       
            if 'export POINTWISE_DIRECTORY=' in data_myfile[line]:
                data_myfile[line] = 'export POINTWISE_DIRECTORY=' + '"'+ current + mesh_path + dir_name + '"' + '\n'       
        
        with open(current + '/pointwise_directory.sh', 'w') as myfile:
            myfile.writelines(data_myfile)
        
        # copy pointwise_directory.sh to home
        shutil.copyfile(current + '/pointwise_directory.sh', home + '/pointwise_directory.sh')  
        
        # make file executable
        subprocess.call(['chmod', '755', home + '/pointwise_directory.sh'])
                
        print("checkMesh_constraint(): written current directory to a file")
                
        # copy pointwise_script.sh to submit script
        shutil.copyfile(current + '/pointwise_script.sh', current + mesh_path + dir_name + '/pointwise_script.sh')        
                 
        # name of the script to submit pointwise with
        bash_pointwise = 'pointwise_script.sh'
        
        # change bash_pointwise in order to run from current directory
        with open(current + mesh_path + dir_name + '/' + bash_pointwise, 'r') as f:
            data = f.readlines()
        
        # read in each line and change the directory location                
        for line in range(len(data)):       
            if '#PBS -o' in data[line]:
                data[line] = '#PBS -o 10.141.0.1:' + current + mesh_path + dir_name + '/log.pointwise' + '\n'
        
            if '#PBS -e' in data[line]:                                                                                                                                                 
                data[line] = '#PBS -e 10.141.0.1:' + current + mesh_path + dir_name + '/err.pointwise' + '\n'
                
        # write the changes to bash_pointwise        
        with open(current + mesh_path + dir_name + '/' + bash_pointwise, 'w') as f:
            f.writelines(data)
        
        # use ssh to send the job to phase 1
        subprocess.call(['ssh','ex-aroberts@login-01.gw4.metoffice.gov.uk', 'source $HOME/pointwise_directory.sh; export PBS_HOME=/cm/shared/apps/pbspro/var/spool; export PBS_EXEC=/cm/shared/apps/pbspro/19.2.4.20190830141245; /cm/shared/apps/pbspro/19.2.4.20190830141245/bin/qsub $POINTWISE_DIRECTORY/pointwise_script.sh;'], cwd=current)
        
        # check if the log files exist
        while not (os.path.exists(current + mesh_path + dir_name + '/' + 'log.pointwise') and os.path.exists(current + mesh_path + dir_name + '/' + 'err.pointwise')):
            time.sleep(1)
            
        print("checkMesh_constraint(): pointwise completed")
               

    else:    
        # run pointwise on isca or locally using Pointwise_path:
        subprocess.call([Pointwise_path, '-b', current + mesh_path + dir_name + '/Hydro_V18_3_tray_APR_grit_pot_parameterised_correct_ellipses_stls_overflow.glf'], cwd=current, \
                          stdout = open(current + mesh_path + dir_name + '/log.pointwise', 'w'), \
                          stderr = open(current + mesh_path + dir_name + '/err.pointwise', 'w'))
    
    # run checkmesh:
    subprocess.call([utility3, '-case', current + mesh_path + dir_name], cwd=current, \
                  stdout = open(current + mesh_path + dir_name + '/log.checkMesh', 'w'), \
                  stderr = open(current + mesh_path + dir_name + '/err.checkMesh', 'w'))
          

    
    # check if the mesh was successful and assign boolean if succeeded              
    with open(current + mesh_path + dir_name + "/log.checkMesh") as f:
        if 'Mesh OK.' in f.read():
            mesh_success = True

    # Convert boolean into integer
    if gap_success == True:
        g_success = 1
    else:
        g_success = 0

    # Convert boolean into integer
    if mesh_success == True:
        m_success = 1
    else:
        m_success = 0

    # Convert boolean into integer
    if ((gap_success == True) and (mesh_success == True)):
        c_success = 1
    else:
        c_success = 0


    # Load the constraints file
    file_constraints = 'constraints.npz'
    data = np.load(current + '/' + file_constraints)

    # Load and set the decision vector
    X = data['arr_0']
    X_current = x

    if X.size != 0:
        X_new = np.vstack((X, X_current))
        #print('appended ', X_current, ' to ', X)
        #print('this gives ', X_new)
    else:
        X_new = X_current
        #print('set ', X_new, ' to ', X_current)


    # Load and set gap constaint success
    G = data['arr_1']
    G_current = g_success

    if G.size != 0:
        G_new = np.vstack((G, G_current))
        #print('appended ', G_current, ' to ', G)
        #print('this gives ', G_new)
    else:
        G_new = G_current
        #print('set ', G_new, ' to ', G_current)


    # Load and set mesh success 
    M = data['arr_2']
    M_current = m_success

    if M.size != 0:
        M_new = np.vstack((M, M_current))
        #print('appended ', C_current, ' to ', C)
        #print('this gives ', C_new)
    else:
        M_new = M_current
        #print('set ', C_new, ' to ', C_current)


    # Load and set convergence success - based on mesh success abd gap constraint success
    C = data['arr_3']
    C_current = c_success

    if C.size != 0:
        C_new = np.vstack((C, C_current))
        #print('appended ', C_current, ' to ', C)
        #print('this gives ', C_new)
    else:
        C_new = C_current
        #print('set ', C_new, ' to ', C_current)

    # Save the decision vector and checkMesh to a file:
    try:     
        np.savez(current + '/' + file_constraints, X_new, G_new, M_new, C_new)
        print('Data saved in file: ', file_constraints)#, ' X ', X_new, ' gap constraint ', G_new, ' checkMesh ', C_new)
    except Exception as e:
        print(e)
        print('Data saving failed.')
                
                               
    # Print failures to the screen
    print('gap_and_checkMesh_constraint(x): minimum_gap_success passed? ', str(gap_success), ' as H = ', H)
    print('gap_and_checkMesh_constraint(x): checkMesh_success passed? ', str(mesh_success))
            
    
    # Success is minimum gap success and checkMesh success:
    success = gap_success and mesh_success
    
    # Handle the failed meshes:
    if(mesh_success == False):
        
        # create mesh failures directory 
        subprocess.call(['mkdir', current + mesh_failures_path + dir_name])
        shutil.copyfile(current + mesh_path + dir_name + '/err.checkMesh', current + mesh_failures_path + dir_name + '/err.checkMesh')
        shutil.copyfile(current + mesh_path + dir_name + '/err.pointwise', current + mesh_failures_path + dir_name + '/err.pointwise')
        shutil.copyfile(current + mesh_path + dir_name + '/log.checkMesh', current + mesh_failures_path + dir_name + '/log.checkMesh')
        shutil.copyfile(current + mesh_path + dir_name + '/log.pointwise', current + mesh_failures_path + dir_name + '/log.pointwise')
        
        # FOR TESTING ONLY I REMOVE THIS LINE:
        
        # remove mesh if there is a mesh failure
        #subprocess.call(['rm', '-r', current + mesh_path + dir_name + '/'])
        #print('centre_constraint(): mesh deleted')

    return success


#################################################
# worker() function used by INITIAL_SAMPLING ONLY
#################################################
    
def worker(input, output):
    for func, args in iter(input.get, 'STOP'):
        result = calculate(func, args)
        output.put(result)

####################################################
# calculate() function used by INITIAL_SAMPLING ONLY
####################################################  
        
def calculate(func, args):
    result = func(*args)
    return '%s says that %s%s = %s' % \
        (current_process().name, func.__name__, args, result)

##############################################
# mul() function used by INITIAL_SAMPLING ONLY
##############################################
        
def mul(d):
    
    if ((environment == 'isca') or (environment == 'isca_test')):
        start_msub = time.time()
        subprocess.call(['msub', '-K', d + '/' + bash], cwd=d, \
              stdout = open(d + '/log.subprocess', 'w'), \
              stderr = open(d + '/err.subprocess', 'w'))

    # submit job using qsub if on isambard:
    elif ((environment == 'isambard') or (environment == 'isambard_test')):
        start_msub = time.time()
        subprocess.call(['qsub', '-W', 'block=true', d + '/' + bash], cwd=d, \
              stdout = open(d + '/log.subprocess', 'w'), \
              stderr = open(d + '/err.subprocess', 'w'))

        
    else:
        start_msub = time.time()
        subprocess.call(['sh', d + '/' + bash], cwd=d, \
              stdout = open(d + '/log.subprocess', 'w'), \
              stderr = open(d + '/err.subprocess', 'w'))
        
    return "Done time," + str(time.time() - start_msub)

################################################
# queue() function used by INITIAL_SAMPLING ONLY
################################################
    
def queue():
    
    filedirs = initialisation()
            
    task_list = [(mul, (d,)) for d in filedirs]    
    
    # Create queues
    task_queue = Queue()
    done_queue = Queue()
    
    # Submit tasks
    for task in task_list:
        task_queue.put(task)

    # Start worker processes
    for i in range(no_of_nodes):
        Process(target=worker, args=(task_queue, done_queue)).start()    
    
    # Get and print results
    print('Unordered results:')
    for i in range(len(task_list)):
        print('\t', done_queue.get())
    
    # Tell child processes to stop
    for i in range(no_of_nodes):
        task_queue.put('STOP')

##################################################
# no_queue() FUNCTION USED BY INITAL_SAMPLING ONLY
##################################################

def no_queue():
    """
    Submit the job to the relevant queue without a python queue.

    Parameters.
    -----------
    There are no parameters. 

    Does not return anything.         
    """
    filedirs = initialisation()
    
    # submit job using moab if on isca:
    if ((environment == 'isca') or (environment == 'isca_test')):
        for d in range(len(filedirs)):
            subprocess.Popen(['msub', filedirs[d] + '/' + bash], cwd=filedirs[d], \
                  stdout = open(filedirs[d] + '/log.subprocess', 'w'), \
                  stderr = open(filedirs[d] + '/err.subprocess', 'w'))
    
    # submit job using qsub if on isambard:
    elif ((environment == 'isambard') or (environment == 'isambard_test')):
        for d in range(len(filedirs)):
            subprocess.Popen(['qsub', filedirs[d] + '/' + bash], cwd=filedirs[d], \
                  stdout = open(filedirs[d] + '/log.subprocess', 'w'), \
                  stderr = open(filedirs[d] + '/err.subprocess', 'w'))
    
    # submit job as a shell script if on local pc:
    else:
        for d in range(len(filedirs)):
            subprocess.Popen(['sh', filedirs[d] + '/' + bash], cwd=filedirs[d], \
                  stdout = open(filedirs[d] + '/log.subprocess', 'w'), \
                  stderr = open(filedirs[d] + '/err.subprocess', 'w'))

########################################################
# initialisation() FUNCTION USED BY INITAL_SAMPLING ONLY
########################################################
            
def initialisation():
    """
    Samples the design space,
    Writes the decision vector to a file
    Creates an empty initial samples file
    Copies and edits the qsub or msub submission script.

    Parameters.
    -----------
    There are no parameters. 

    Returns a list of directories for submission.         
    """
    
    # set the location of the case path
    case_path = "/data/HeadCell/case_local/"
    source_path = "/data/HeadCell/source/."
        
    source_file = 'run_case_initial_sampling.py'
    
    # remove the current case path and create a new case path
    subprocess.call(['rm', '-r', current + case_path])    
    subprocess.call(['mkdir','-p', current + case_path])
    
    # get the lower and upper bounds
    lb, ub = prob.get_decision_boundary() 
    
    print("lb, ub", lb, ub)

    # number of dimensions and samples    
    n_dim = 14
    n_samples = 11*n_dim -1
    
    # read decision vector and write to checkMesh.npz file
    print('Writing empty constraints.npz file...')
    
    # gap and checkMesh is null
    initial_X = []
    initial_H = []
    initial_M = []
    initial_C = []
        
    file_constraints = 'constraints.npz'
    
    subprocess.call(['rm', '-r', current + '/' + file_constraints])
    
    # initial_X is decision vector
    # initial_H is gap constraint
    # initial_M is checkMesh
    # initial_C is convergence

    try:
        np.savez(current + '/' + file_constraints, initial_X, initial_H, initial_M, initial_C)
        print('Data saved in file: ', file_constraints)
    except Exception as e:
        print(e)
        print('Data saving failed.') 
    
    
    
    # take samples for the decision vector, either manual or latin hypercube
    if sampling == 'manual':
        print("initialisation(): number of manual samples", str(9))
        
        # 3 deliberate passes:
        one = np.array([0.5, 0.5, 10.0, 4.0, 4.0]) # 5,145,018 cells, 906 severly orthoginal faces, passes checkMesh Should result in H=3.02 inches
        two = np.array([1.0, 1.0, 20.0, 4.0, 5.0]) # 5,095,425 cells, 1087 severly orthoginal faces, passes checkMesh Should result in H=2.85 inches
        three = np.array([1.5, 1.5, 30.0, 4.0, 6.0]) # 4,993,048 cells, 1331 severly orthoginal faces, passes checkMesh Should result in H=2.67 inches

        # 3 deliberate failures
        four = np.array([4.0, 4.0, 10.0, 8.0, 6.0]) # Should result in H=1.51 inches
        five = np.array([4.0, -4.0, 30.0, 8.0, 4.0]) # Should result in H=1.62 inches
        six = np.array([4.0, -4.0, 45.0, 8.0, 4.0]) # Should result in H=1.76 inches
        
        # Mix of success and failure
        seven = np.array([1.0, 1.0, 10.0, 4.0, 5.0]) # Should result in H=2.83 inches
        eight = np.array([1.5, 1.5, 10.0, 4.0, 6.0]) # Should result in H=2.61 inches
        nine = np.array([1.0, 1.0, 30.0, 4.0, 5.0]) # Should result in H=2.88 inches
      
        all_samples = np.array([one, two, three, four, five, six, seven, eight, nine])
                
        samples = []
        
        for x in all_samples:
            constraint_success = gap_and_checkMesh_constraint(x, layout)
            # It cannot run meshes that fail or if gaps are too small
            if constraint_success == True:
                samples.append(x)
    else:
        # It cannot run meshes that fail or if gaps are too small
        print("initialisation(): number of Latin Hypercube samples", str(n_samples))
        samples = lhs_initial_samples(n_dim, ub, lb, n_samples, cfunc=gap_and_checkMesh_constraint, cargs=(layout,), ckwargs={})
    

    
    # create filedirs
    filedirs = []
    
    # loop through the list to create directories:
    for s in samples:
        
        # create a working directory from the sample:
        dir_name = "_"
        for j in range(len(s)):
            dir_name += "{0:.4f}_".format(s[j])
        
        # replace any directories containing []
        dir_name = dir_name.replace("[","")
        dir_name = dir_name.replace("]","")
        
        # add the name to a list of directories
        filedirs.append(current + case_path + dir_name)
        
        # create the directory from the last in the list and 
        subprocess.call(['mkdir', filedirs[-1] + '/'])
        
        # copy all source files into the newly created directory
        subprocess.call(['cp', '-r', current + source_path, filedirs[-1]])
        
        # copy run_case.py to that directory
        subprocess.call(['cp', '-r', current + '/' + source_file, filedirs[-1] + '/'])
           
        # write the decision vector to a file        
        with open(filedirs[-1] + "/decision_vector.txt", "a") as myfile:
            for i in range(0,len(s)):
                myfile.write(str(s[i])+ '\n')
        print("initialisation(): written decision vector to a file")
    
    
    # read decision vector and write to npz file
    print('Writing empty initial_samples.npz file...')
    
    # hyper volume improvement is null
    hpv = []
    initial_time = 0
    initial_X = []
    initial_Y = []
    initial_convergence = []
    
    # the name of the sim_file is initial_samples.npz    
    sim_file = 'initial_samples.npz'
    
    # remove npz file if it exists
    subprocess.call(['rm', '-r', current + '/' + sim_file]) 
    
    # initial_X is decision vector
    # initial_Y is 2 objectives
    # hpv is hypervolume improvement
    # initial_time is zero
    # initial_convergence is convergence failure  
    try:
        np.savez(current + '/' + sim_file, initial_X, initial_Y, hpv, initial_time, initial_convergence)
        print('Data saved in file: ', sim_file)
    except Exception as e:
        print(e)
        print('Data saving failed.')


    # decide environment and adjust bash script for running.
    if ((environment == 'isca') or (environment == 'isca_test')):
        # if the environment is isca
        for d in range(len(filedirs)):
            
            # copy the bash run script to the run directory
            subprocess.call(['cp', current + '/' + bash, filedirs[d] + '/'])
                        
            # open the bash script for running
            with open(filedirs[d] + '/' + bash, 'r') as f:
                data = f.readlines()
                
            # change the line to the correct directory
            for line in range(len(data)):       
                if '#PBS -d' in data[line]:
                    data[line] = '#PBS -d '+ filedirs[d]+'/' + '\n'
            
                if 'python -u' in data[line]:
                    data[line] = 'python -u run_case_initial_sampling.py > log.python 2>&1' + '\n'
            
            #write the lines to the file        
            with open(filedirs[d] + '/' + bash, 'w') as f:
                f.writelines(data)
                
    elif ((environment == 'isambard') or (environment == 'isambard_test')):
        # if the environment is isambard
        for d in range(len(filedirs)):
            
            # copy the bash run script to the run directory
            subprocess.call(['cp', current + '/' + bash, filedirs[d] + '/'])
                        
            # open the bash script for running
            with open(filedirs[d] + '/' + bash, 'r') as f:
                data = f.readlines()
                
            # change the line to the correct directory
            for line in range(len(data)):       
                if '#PBS -d' in data[line]:
                    data[line] = '#PBS -d '+ filedirs[d]+'/' + '\n'
            
                if 'python -u' in data[line]:
                    data[line] = 'python -u run_case_initial_sampling.py > log.python 2>&1' + '\n'
            
            #write the lines to the file        
            with open(filedirs[d] + '/' + bash, 'w') as f:
                f.writelines(data)
                
    else:
        # if the environment is local:    
        for d in range(len(filedirs)):
            # copy the bash script to the local directory
            subprocess.call(['cp', current + '/' + bash, filedirs[d] + '/'])
            
    print('Total number of simulations, ', len(filedirs))
    print('All directories created.')

    return filedirs


####################################################################
# mesh_and_two_objectives(x, layout) FUNCTION USED BY OPTIMISER ONLY
####################################################################

# define the cost function: this can take 11hours 10minutes on ISCA (say 12 hours) -  27 cases: 13.5 days (I gave it 2 weeks)
def mesh_and_two_objectives(x, layout):
    """
    A mock test function that accepts a decision variable and updates the 
    given layout and returns a function value.
    """
    print("hydro_1D(x, layout): evaluating cost function")
    
    ##################################################
    # CREATE CASE DIRECTORY AND EDIT SUBMISSION SCRIPT
    ##################################################
    filedirs = BO_initialisation(x)
      
    #################
    # MESH GENERATION
    #################
    # Pointwise, checkMesh - it should always pass the gap constraint
        # x is the decision vector
        # layout is the object for the generation of the pointwise files
    mesh_generation_succeeded = gap_and_checkMesh_constraint(x, layout)
    
    # mesh generation succeeded means gap constraint is always passed
    # (the optimiser uses gap_contraint to look for solutions)
    
    ###################################
    # JOB SUBMISSION VIA run_case_BO.py
    ###################################
    
            
    if (mesh_generation_succeeded):
        # It will only submit jobs if it passed checkMesh
        BO_submit(filedirs[-1])
        
        ###############################
        # READ IN OBJECTIVES FROM FILES
        ###############################
        efficiency = np.loadtxt(filedirs[-1] + '/efficiency.txt')
        pressure_overflow = np.loadtxt(filedirs[-1] + '/pressure_overflow.txt')
        
    else:
        # WARNING #####################################
        # Fine for serial - need to change for parallel
        # WARNING #####################################
        f = open(current + "/sim_file_BO.txt", "r")
        file_name = f.read()
        data=np.load(file_name)
        
    	# Smallest collection efficiency:
        collection_efficiency=data['arr_1'][: , 0]
        # This will actually be max because collection_efficiency is recorded as negative
        worst_collection_efficiency = max(collection_efficiency)
    
    	# Largest pressure drop:
        pressure_drop=data['arr_1'][: , 1]
        worst_pressure_drop = max(pressure_drop)
        
        # If checkMesh failed did not succeed, poor values are given
        with open(filedirs[-1] + "/pressure_overflow.txt", "a") as myfile_pressure_overflow:
            myfile_pressure_overflow.write(str(float(worst_pressure_drop))+ '\n')
        print("mesh_and_two_objectives(): checkMesh failure, so written worst pressure_overflow to a file")
        
        with open(filedirs[-1] + "/efficiency.txt", "a") as myfile_efficiency:
            myfile_efficiency.write(str(float(worst_collection_efficiency))+ '\n')
        print("mesh_and_two_objectives(): checkMesh failure, no convergence, so written worst efficiency to a file")
        
        # It also fails to converge if checkMesh fails, so record this
        with open(filedirs[-1] + "/convergence.txt", "a") as myfile_convergence:
            myfile_convergence.write((str(0)) + '\n')
        print("OpenFOAM(): written convergence failure to a file") 
        
        ########################################################################################################################        
        # Need to update the constraints.npz to reflect the fact that checkMesh failed - so no convergence either by implication
        ########################################################################################################################
        
        # decision_vector, convergence:
        decision_vector = np.loadtxt(filedirs[-1] + '/decision_vector.txt')
        convergence = np.loadtxt(filedirs[-1] + '/convergence.txt')
            
        # load the constraints.npz file:
        p = Path(filedirs[-1] + "/decision_vector.txt").parents[4]
        print("main(): path to constraints file:", p)
        constraints_file = "constraints.npz"
        b_lock = fasteners.InterProcessLock(str(p) + '/' + constraints_file)
        
        # load the data and write the new data
        while True:
            unlocked_b = b_lock.acquire(blocking=False)
            try:
                if unlocked_b:
                    print('I have the constraints file lock')
                    time.sleep(20)
                    
                    data = np.load(str(p) + '/' + constraints_file)
                    
                    D = data['arr_0'] # decision vector
                    G = data['arr_1'] # gap constraint
                    M = data['arr_2'] # mesh constraint
                    Z = data['arr_3'] # convergence constraint
                    
                    D_current = decision_vector
                    Z_current = convergence
                    
                    # get the index of the row where the decision vector matches
                    index = np.where(np.all(D==D_current,axis=1))[0]
                    
                    # put the correct value for convergence into the array
                    np.put(Z, index, Z_current)
    
                    try:     
                        np.savez(str(p) + '/' + constraints_file, D, G, M, Z)
                        print('Data saved in file: ', constraints_file, ' directory ', str(p), ' D ', D, ' G ', G, ' M ', M, ' Z ', Z)
                    except Exception as ex:
                        print(ex)
                        print('Data saving failed to constraints file.')
                    
                else:
                    print('I do not have the constraints file lock')
                    time.sleep(20)
            finally:
                if unlocked_b:
                    b_lock.release()
                    break             
        
        ###############################
        # READ IN OBJECTIVES FROM FILES
        ###############################
        efficiency = np.loadtxt(filedirs[-1] + '/efficiency.txt')
        pressure_overflow = np.loadtxt(filedirs[-1] + '/pressure_overflow.txt')
        
        ################################################################
        # Also delete the directory that corresponds with the false mesh
        ################################################################
        
        subprocess.call(['rm', '-rf', filedirs[-1]])
        print('Removed the case directory corresponding with the failed mesh')
        
    ###########################################################
    # MAXIMISE COLLECTION EFFICIENCY, MINIMISE MECH ENERGY LOSS
    ###########################################################
    # -y means minimisation of a negative number - i.e. maximising a positive number:
    # +y means minimisation of a positive number
    # efficiency is written to files as a negative number, so is automatically negative
    return efficiency, pressure_overflow




def BO_initialisation(x):
    """
    Samples the design space,
    Writes the decision vector to a file
    Creates an empty initial samples file
    Copies and edits the qsub or msub submission script.

    Parameters.
    -----------
    There are no parameters. 

    Returns a list of directories for submission.         
    """
    
    # set the location of the case path
    case_path = "/data/HeadCell/case_local/"
    source_path = "/data/HeadCell/source/."
        
    source_file = 'run_case_BO.py'
        
    # get the lower and upper bounds
    lb, ub = prob.get_decision_boundary() 
    
    print("lb, ub", lb, ub)   
    
    # Directory name
    dir_name = "_"
    for j in range(len(x)):
        dir_name += "{0:.4f}_".format(x[j])
    
    # replace any directories containing []
    dir_name = dir_name.replace("[","")
    dir_name = dir_name.replace("]","")
    
    # create the directory
    subprocess.call(['mkdir', current + case_path + dir_name + '/'])
    
    # copy all source files into the newly created directory
    subprocess.call(['cp', '-r', current + source_path, current + case_path + dir_name])
    
    # copy run_case.py to that directory
    subprocess.call(['cp', '-r', current + '/' + source_file, current + case_path + dir_name + '/'])
       
    # write the decision vector to a file        
    with open(current + case_path + dir_name + "/decision_vector.txt", "a") as myfile:
        for i in range(0,len(x)):
            myfile.write(str(x[i])+ '\n')
    print("initialisation(): written decision vector to a file")
   
    ############################################
    # There is only ever one directory at a time
    ############################################
    
    # create filedirs
    filedirs = []
    
    # add the name to a list of directories
    filedirs.append(current + case_path + dir_name)
        
    # decide environment and adjust bash script for running.
    if ((environment == 'isca') or (environment == 'isca_test')):
        # if the environment is isca
            
        # copy the bash run script to the run directory
        subprocess.call(['cp', current + '/' + bash, current + case_path + dir_name + '/'])
                    
        # open the bash script for running
        with open(current + case_path + dir_name + '/' + bash, 'r') as f:
            data = f.readlines()
            
        # change the line to the correct directory
        for line in range(len(data)):       
            if '#PBS -d' in data[line]:
                data[line] = '#PBS -d '+ current + case_path + dir_name +'/' + '\n'
        
            if 'python -u' in data[line]:
                data[line] = 'python -u run_case_BO.py > log.python 2>&1' + '\n'
        
        #write the lines to the file        
        with open(current + case_path + dir_name + '/' + bash, 'w') as f:
            f.writelines(data)
                
    elif ((environment == 'isambard') or (environment == 'isambard_test')):
        # if the environment is isambard
            
        # copy the bash run script to the run directory
        subprocess.call(['cp', current + '/' + bash, current + case_path + dir_name + '/'])
                    
        # open the bash script for running
        with open(current + case_path + dir_name + '/' + bash, 'r') as f:
            data = f.readlines()
            
        # change the line to the correct directory
        for line in range(len(data)):       
            if '#PBS -d' in data[line]:
                data[line] = '#PBS -d '+ current + case_path + dir_name +'/' + '\n'
                
            if 'python -u' in data[line]:
                data[line] = 'python -u run_case_BO.py > log.python 2>&1' + '\n'
                
        #write the lines to the file        
        with open(current + case_path + dir_name + '/' + bash, 'w') as f:
            f.writelines(data)
            
    else:
        # if environment is local
        subprocess.call(['cp', current + '/' + bash, current + case_path + dir_name + '/'])
            
    print('Directory created.')

    return filedirs



def BO_submit(d):
    """
    Submit the job to the relevant queue without a python queue 
    Copy of no_queue without a list of directories - i.e. it just submits one run.

    Input
    -----
    A single directory. 
    
    Output
    ------
    Does not return anything.         
    """
    # submit job using msub if on isca:
    if ((environment == 'isca') or (environment == 'isca_test')):
        start_msub = time.time()
        subprocess.call(['msub', '-K', d + '/' + bash], cwd=d, \
              stdout = open(d + '/log.subprocess', 'w'), \
              stderr = open(d + '/err.subprocess', 'w'))

    # submit job using qsub if on isambard:
    elif ((environment == 'isambard') or (environment == 'isambard_test')):
        start_msub = time.time()
        subprocess.call(['qsub', '-W', 'block=true', d + '/' + bash], cwd=d, \
              stdout = open(d + '/log.subprocess', 'w'), \
              stderr = open(d + '/err.subprocess', 'w'))

    # submit job using shell locally:    
    else:
        start_msub = time.time()
        subprocess.call(['sh', d + '/' + bash], cwd=d, \
              stdout = open(d + '/log.subprocess', 'w'), \
              stderr = open(d + '/err.subprocess', 'w'))
        
    return "Done time," + str(time.time() - start_msub)




###########################################################
# gap_constraint(x, layout) FUNCTION USED BY OPTIMISER ONLY
###########################################################

def gap_constraint(x, layout):
    """
    Checks only the gap constraint: writes no constraints.npz file.

    Parameters.
    -----------
    x (numpy array): the decision vector. 
    layout (Ellipse object): the object for the generation of the pointwise files. 

    Returns whether the constraint was successfully passed.         
    """
    # get the current directory
    #current = os.getcwd()
    
    # defaults for success:
    minimum_gap_success = True

    # location of centre of ellipse:
    bottom_x_centre = x[0]
    bottom_z_centre = x[1]
    
    # rotation angle:
    rot_angle = x[2]*(np.pi/180.0)
    
    # ellipse sizes:
    a = x[3]
    b = x[4]
    
    # create space for corners and gaps:
    corners = np.zeros((4,2))
    gaps = np.zeros(4)
    
    # the bottom of the tray:
    y_bottom = -14.9375
    
    # very top of tray: 
    p1 = np.array([0, 6.0625, 0])
    
    # very bottom of tray:
    p2 = np.array([bottom_x_centre, y_bottom, bottom_z_centre])
    
    # the radii at the first tray layer:
    a_top = layout.get_ellipse_radii(21.25, a, -2.34375)
    b_top = layout.get_ellipse_radii(21.25, b, -2.34375)
    
    # the points at the first tray layer:
    c_top = layout.get_ellipse_points(p1, p2, -2.34375)
    
    # the angle at the first tray layer:
    angle_top = layout.get_ellipse_angles(rot_angle, -2.34375)
    
    # obtain the four corners:
    for i in range(1,5):
        angles = np.linspace(start=0.5*i*np.pi, stop=0.5*(i-1)*np.pi, num=1, endpoint=True)
        corners[i-1,:] = layout.generate_ellipse(c_top, a_top, b_top, angles, theta=angle_top+np.pi, n=0)

    # compute horizontal gaps:
    for j in range(0,4):
        gaps[j] = 21.25 - np.sqrt((corners[j,0])**2 + (corners[j,1])**2)
    
    # minimim horizontal gap:
    min_gap=min(gaps)

    # get the index of the minimum horizontal gap and the maximum x-extent:
    index = np.where(gaps == min_gap)
    x_extent_vector = abs(corners[index])
    x_extent= np.amax(x_extent_vector)
    
    # Definition of the x-y plane locations
    x1 = 21.25
    x2 = x_extent
    x3 = x_extent
    x4 = 21.25
    
    y1 = -2.9375
    y2 = -2.34375
    y3 = -6.59375
    y4 = 1.3125
    
    # Hero's formula for the normal gap
    A = y2 - y3
    B = np.sqrt((y4 - y2)**2 + (x4 - x2)**2)
    P = np.sqrt((x1 - x2)**2 + (y1 - y2)**2)
    S = (A+B+P)/2
    Area = np.sqrt(S*(S-A)*(S-B)*(S-P))
    H = 2*Area/B
    
    # constraints:
    if (H < 2.0):
        # the tray gap is less than 2 inches:
        minimum_gap_success = False
    
    return minimum_gap_success
    




if __name__ == '__main__':
    
    current = os.getcwd()
    
    # initial samples file
    sim_file = 'initial_samples.npz'
    initial_samples_exist = os.path.exists(current + '/' + sim_file)
    print("main(): The initial samples file exists?", initial_samples_exist)
    
    # constraint file corresponding to initial samples file
    con_file = 'constraints.npz'
    constraints_exist = os.path.exists(current + '/' + con_file)
    print("main(): The constraints file exists?", constraints_exist)
    
    # This needs to be altered to match any existing file
    sim_restart_file = 'mesh_and_two_objectives_HypI_b90s54_r2020-02-10.npz'
    sim_restart_file_exist = os.path.exists(current + '/' + sim_restart_file)
    print("main(): The sim_restart file exists?", sim_restart_file_exist)
    
    # Has to have the same samples as sim_restart_file (plus any failures)
    con_restart_file = 'constraints.npz'
    con_restart_file_exist = os.path.exists(current + '/' + con_restart_file)
    print("main(): The con_restart file exists?", con_restart_file_exist)
    

    if(initial_samples_exist and constraints_exist):
        
        print("main(): Initial samples file and constraints file exists")
        print("main(): Runnning the optimiser, with initial samples")
        
        ###############################################################################
        # START OF INITIAL SAMPLES EDITING - DELETE ANY VECTORS THAT FAILED TO CONVERGE
        ###############################################################################
        
        # Load .npz file
        data = np.load(current + '/' + sim_file)
                
        # Convergence failures
        A=data['arr_4']
        index = np.argwhere(A == 0)
        row = []
        
        print("main(): ", len(index), " samples failed to converge")
        
        for i in range(len(index)):
            row.append(index[i][0])
        
        A4 = np.delete(A, (row), axis = 0)
        
        # Decision vectors
        A=data['arr_0']
        A0 = np.delete(A, (row), axis = 0)
        
        # Objectives
        A=data['arr_1']
        A1 = np.delete(A, (row), axis = 0)
        
        # Hypervolume improvement is empty (arr_2)
        A2=data['arr_2']
        
        # Total simulation time is zero (arr_3)
        A3=data['arr_3']

        # Save initial samples file        
        sim_file = 'initial_samples_deleted_failed_samples.npz'
        np.savez(current + '/' + sim_file, A0, A1, A2, A3, A4)
        print("main(): ", len(index), " samples have been deleted")
        
        #####################
        # START THE OPTIMISER
        #####################
        
        # Start time
        start_sim = time.time()
        
        # Seed 
        seed = 1005
        np.random.seed(seed)
        
        # Problem object
        prob = HeadCell({}) 
    
        # Sets values and removes olds case directory
        subprocess.call(['rm', '-r', prob.case_path])
        subprocess.call(['rm', '-r', prob.mesh_path])
        subprocess.call(['rm', '-r', prob.mesh_failures_path])
        subprocess.call(['mkdir', '-p', prob.case_path])
        #subprocess.call(['cp', '-r', prob.source_case, prob.case_path])
        subprocess.call(['mkdir', '-p', prob.mesh_path])
        subprocess.call(['mkdir', '-p', prob.mesh_failures_path])
        print("main(): removed old case, copied new case and created new mesh directories")
        
        # Get upper and lower bounds
        lb, ub = prob.get_decision_boundary() 
        print("lb", lb)
        print("ub", ub)
    
        # Simulation ID
        today = str(date.today())
        sim_id = today
        
        # Samples and budget
        if ((environment == 'isambard_test') or (environment == 'isca_test')):
            n_samples = 5               # initial samples
            budget = 10                 # only go as far as 10 
        else:
            n_samples = 54              # (11n - 1) 
            budget = 90                 # (11n - 1) + (2/3)(11n-1)

        # Layout object
        layout = Ellipse(lb, ub)
        
        method_name = 'HypI'
        run = sim_id
        
        sim_file_BO = mesh_and_two_objectives.__name__ + '_' + method_name + \
            '_b' + str(budget) + 's' + str(n_samples) + '_r' + str(run) + '.npz'
        
        with open(current + "/sim_file_BO.txt", "w") as sim_file_BO_file:
            sim_file_BO_file.write(sim_file_BO)
        
        # Create EMO object
        print("main(): EMO function start")
        res = IscaOpt.Optimiser.EMO(func=mesh_and_two_objectives, fargs=(layout,), fkwargs={}, \
                            cfunc=gap_constraint, cargs=(layout,), ckwargs={}, \
                            settings={'n_dim':5,\
                            'n_obj':2, 'run': sim_id,\
                            'method_name':method_name, 'lb':lb, 'ub':ub, \
                            'n_samples':n_samples, 'budget':budget, \
                            'visualise':False, \
                            'init_file':sim_file})
        
        print("main(): Time taken (minutes), ", (time.time()-start_sim)/60.0)
    
    
    elif(sim_restart_file_exist and con_restart_file_exist):
        
        print("main(): Samples restart file and constraints restart file exists")
        print("main(): Runnning the optimiser, with restart files")
                
        #####################
        # START THE OPTIMISER
        #####################
        
        # Start time
        start_sim = time.time()
        
        # Seed 
        seed = 1005
        np.random.seed(seed)
        
        # Problem object
        prob = HeadCell({}) 
    
        # Sets values and removes olds case directory
        subprocess.call(['rm', '-r', prob.case_path])
        subprocess.call(['rm', '-r', prob.mesh_path])
        subprocess.call(['rm', '-r', prob.mesh_failures_path])
        subprocess.call(['mkdir', '-p', prob.case_path])
        #subprocess.call(['cp', '-r', prob.source_case, prob.case_path])
        subprocess.call(['mkdir', '-p', prob.mesh_path])
        subprocess.call(['mkdir', '-p', prob.mesh_failures_path])
        print("main(): removed old case, copied new case and created new mesh directories")
        
        # Get upper and lower bounds
        lb, ub = prob.get_decision_boundary() 
        print("lb", lb)
        print("ub", ub)
    
        # Simulation ID
        today = str(date.today())
        sim_id = today
        
        # Samples and budget
        if ((environment == 'isambard_test') or (environment == 'isca_test')):
            n_samples = 5               # initial samples
            budget = 10                 # only go as far as 10 
        else:
            n_samples = 54              # (11n - 1) 
            budget = 90                 # (11n - 1) + (2/3)(11n-1)

        # Layout object
        layout = Ellipse(lb, ub)
        
        method_name = 'HypI'
        run = sim_id
        
        sim_file_BO = mesh_and_two_objectives.__name__ + '_' + method_name + \
            '_b' + str(budget) + 's' + str(n_samples) + '_r' + str(run) + '.npz'
        
        with open(current + "/sim_file_BO.txt", "w") as sim_file_BO_file:
            sim_file_BO_file.write(sim_file_BO)
        
        # Create EMO object
        print("main(): EMO function start")
        res = IscaOpt.Optimiser.EMO(func=mesh_and_two_objectives, fargs=(layout,), fkwargs={}, \
                            cfunc=gap_constraint, cargs=(layout,), ckwargs={}, \
                            settings={'n_dim':5,\
                            'n_obj':2, 'run': sim_id,\
                            'method_name':method_name, 'lb':lb, 'ub':ub, \
                            'n_samples':n_samples, 'budget':budget, \
                            'visualise':False, \
                            'init_file':sim_restart_file})
        
        print("main(): Time taken (minutes), ", (time.time()-start_sim)/60.0)
    

    else:
        
        # Indicate there is no initial samples file
        print("main(): Initial samples file and/or constraints file doesn't exist")
        print("main(): Runnning initial samples first, then the optimiser")
        
        ##########################
        # START OF INITIAL SAMPLES
        ##########################
        
        # Start time
        start_sim = time.time()
        
        # Seed       
        seed = 1005
        np.random.seed(seed)
        
        # Problem object
        prob = HeadCell({})
    
        # Sets values and removes olds case directory
        subprocess.call(['rm', '-r', prob.case_path])
        subprocess.call(['rm', '-r', prob.mesh_path])
        subprocess.call(['rm', '-r', prob.mesh_failures_path])
        subprocess.call(['mkdir','-p', prob.case_path])
        #subprocess.call(['cp', '-r', prob.source_case, prob.case_path])
        subprocess.call(['mkdir', '-p', prob.mesh_path])
        subprocess.call(['mkdir', '-p', prob.mesh_failures_path])
        print("main(): removed old case, copied new case and created new mesh directories")
        
        # Get upper and lower bounds:
        lb, ub = prob.get_decision_boundary() 
        print("main(): lb", lb)
        print("main(): ub", ub)
        
        # Layout is needed for initialisation
        layout = Ellipse(lb, ub)
        
        # Use this line if testing grid generation
        initialisation()
        
        # Uncomment this if there is no queue
        #no_queue()
        
        # This creates a queue
#        freeze_support()
#        queue()
        
        # Print values to the screen 
        print("main(): Maximum number of simultaneous runs, ", no_of_nodes)
        print("main(): Time taken (minutes), ", (time.time()-start_sim)/60.0)
